﻿using System;

namespace BLETesting
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var watcher = new BLE_AdvertisementListener.BLE_AdWatcher();
            var serial = new BLE_AdvertisementListener.SerialTx();
            serial.SerialPorts_Init();
            //debug code
            watcher.StartedListening += () =>
            {
                Console.WriteLine("Started Listening");
            };
            //debug code
            watcher.StoppedListening += () =>
            {
                Console.WriteLine("Stopped Listening");
            };

            watcher.StartListening();
            
            watcher.NewDeviceDiscovered += (device) =>
            {
                // lot more understanding of how to use threads properly to ensure they run simulataneously
                    Console.WriteLine($" New Device Discovred: {device} Address: {device.Address} Advertisement Time : {device.BroadcastTime}  ID : {device.DeviceID})");
                    serial.RunSerialTx_Rx(device.Address.ToString());
                if (device.Name == "Cabg")
                    {
                    Console.WriteLine($"Device Details : {device.Name} \n Device Signal Strength {device.SignalStrengthInDB}\n Device Found at {device.BroadcastTime}\n Device ID {device.DeviceID} \n");
                    
                    System.Threading.Thread.Sleep(1000);
                    serial.RunSerialTx_Rx(device.Address.ToString());
                    System.Threading.Thread.Sleep(1000);
                    serial.RunSerialTx_Rx(device.Name.ToString());
                    System.Threading.Thread.Sleep(1000);
                    serial.RunSerialTx_Rx(device.SignalStrengthInDB.ToString());
                    System.Threading.Thread.Sleep(1000);
                    serial.RunSerialTx_Rx(device.BroadcastTime.ToString());
                    System.Threading.Thread.Sleep(1000);
                    watcher.StopListening();
                    watcher.PairToDevice(device.Address);

                }

                System.Threading.Thread.Sleep(1000);

                // watcher.PrintDictionary();

            };
            
            Console.ReadLine();

        }
    }
}
