﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;

namespace BLE_AdvertisementListener
{

    public class SerialTx
    {
        public event SerialDataReceivedEventHandler DataReceived;

        public SerialPort TxSerialPort = new SerialPort();
        public SerialPort RxSerialPort = new SerialPort();

        public void SerialPorts_Init()
        {
            // scanning for serial ports
            string[] ports = SerialPort.GetPortNames();
            Console.WriteLine("The following serial ports were found:");

            // Display each port name to the console.
            foreach (string port in ports)
            {
                if (port.Equals("COM1"))
                {
                    TxSerialPort.PortName = port.ToString();
                    Console.WriteLine($"Found {port} assigning it as Tx {TxSerialPort.PortName}");
                    Console.WriteLine($"Baudrate {TxSerialPort.PortName} is {TxSerialPort.BaudRate}");
                    Console.WriteLine($"Parity {TxSerialPort.PortName} is {TxSerialPort.Parity}");
                    Console.WriteLine($"DataBits {TxSerialPort.PortName} is {TxSerialPort.DataBits}");
                    Console.WriteLine($"StopBits {TxSerialPort.PortName} is {TxSerialPort.StopBits}");
                    Console.WriteLine($"Handshake {TxSerialPort.PortName} is {TxSerialPort.Handshake}");
                    TxSerialPort.RtsEnable = true;

                }
                if (port.Equals("COM2"))
                {
                    RxSerialPort.DataReceived += new SerialDataReceivedEventHandler(DataReceivedHandler);
                    RxSerialPort.PortName = port.ToString();
                    Console.WriteLine($"Found {port} assigning it as Rx {RxSerialPort.PortName}");
                    Console.WriteLine($"Baudrate {RxSerialPort.PortName} is {RxSerialPort.BaudRate}");
                    Console.WriteLine($"Parity {RxSerialPort.PortName} is {RxSerialPort.Parity}");
                    Console.WriteLine($"DataBits {RxSerialPort.PortName} is {RxSerialPort.DataBits}");
                    Console.WriteLine($"StopBits {RxSerialPort.PortName} is {RxSerialPort.StopBits}");
                    Console.WriteLine($"Handshake {RxSerialPort.PortName} is {RxSerialPort.Handshake}");

                    RxSerialPort.RtsEnable = true;

                }
            }
            TxSerialPort.Open();
            RxSerialPort.Open();
        }

        public void RunSerialTx_Rx(string data)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(data);
            Int32 i = 0;
            while (i < bytes.Length)
            {
                TxSerialPort.Write($"{bytes[i]}");
                i++;
            }


        }
        static IEnumerable<byte> GetBytesFromByteString(string s)
        {
            for (int index = 0; index < s.Length-2; index += 2)
            {
                yield return Convert.ToByte(s.Substring(index, 2));
            }
        }
        static void DataReceivedHandler(
                        object sender,
                        SerialDataReceivedEventArgs e)
        {
            SerialPort sp = (SerialPort)sender;
            // this will not work when passing chunks of data...use the event
            string indata = sp.ReadExisting();
            var bytearray = GetBytesFromByteString(indata).ToArray();
            string Display = Encoding.ASCII.GetString(bytearray);
            Console.WriteLine($"Data Received:{Display}");

        }
        #region Constructor
        public SerialTx()
            {
            }
        #endregion
    }
    
}
