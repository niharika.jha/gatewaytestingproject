﻿using System;
using System.Collections.Generic;
using System.Linq;
using Windows.Foundation;
using Windows.Devices.Bluetooth.Advertisement;
using Windows.Devices.Bluetooth;
using Windows.Devices.Enumeration;
using System.IO.Ports;
using System.Text;
using System.Threading.Tasks;
using static System.WindowsRuntimeSystemExtensions;

namespace BLE_AdvertisementListener
{
     // makes use of inbuilt Windows Functions to discover BLE Devices
    public class BLE_AdWatcher
    {
        #region Private Members
        // Advertisement Watcher
        private readonly BluetoothLEAdvertisementWatcher mWatcher;
        // List of Discovered Devices
        private  Dictionary<ulong, BLEDevice> mDiscoveredDevices = new Dictionary<ulong, BLEDevice>();

        // A thread lock object for this class
        private readonly object mThreadLock = new object();
        #endregion
        #region Public Events
        // triggered when Bluetooth Watcher Stops Listening
        public event Action StoppedListening = () => { };
        // triggered when Bluetooth Watcher Starts Listening
        public event Action StartedListening = () => { };
        // triggered when new Device is discovered
        public event Action <BLEDevice> NewDeviceDiscovered = (device) => { };

        // triggered when any Device is discovered
        public event Action<BLEDevice> DeviceDiscovered = (device) => { };

        public event SerialDataReceivedEventHandler DataReceived;

        #endregion
        #region Public Properties
        // Indicates if watcher is listening
        public bool Listening => mWatcher.Status == BluetoothLEAdvertisementWatcherStatus.Started;

        // sending to an xlsx reader
        public IReadOnlyCollection <BLEDevice> DiscoveredDevices
        {
            get
            { 
                // Ensuring that when we are querying values and listing them , it is thread safe
                // this is not the correct behaviour , should be looked into
                lock(mThreadLock)
                {
                    return mDiscoveredDevices.Values.ToList().AsReadOnly();
                }
            }
        }
        
        #endregion
        #region Constructor
        // default Constructor
        public BLE_AdWatcher()
        {
            mWatcher = new BluetoothLEAdvertisementWatcher
            {
                // Put the Client into Active Scanning Mode. Other properties includes spanning time
                // filter, status, signal strength, etc
                ScanningMode = BluetoothLEScanningMode.Active
            };
            // Listen for new Advertisements
            //getback on this--let me find a better explanation for this
            mWatcher.Received += WatcherAdvertisementReceived;
            mWatcher.Stopped += (watcher, e) =>
            {
                StoppedListening();
            };
           
        }

        // Listens out for Watcher Advertisments
        private void WatcherAdvertisementReceived(BluetoothLEAdvertisementWatcher sender, BluetoothLEAdvertisementReceivedEventArgs args)
        {
            BLEDevice device ;
            /*Debug Print Lines*/
           //Console.WriteLine("Arguments Passed");
           //Console.WriteLine(args.BluetoothAddress.ToString());
           //Console.WriteLine(args.Timestamp.ToString());
           //Console.WriteLine(args.AdvertisementType.ToString());

            // check if there is a new device and is it already present in dictionary
            var newDiscovery = !mDiscoveredDevices.ContainsKey(args.BluetoothAddress);
            lock (mThreadLock)
            {
                // get name will be useful for trimming the list
                var name = args.Advertisement.LocalName;// --> //redundant for trimming the list

                device = new BLEDevice();
                device.BroadcastTime = args.Timestamp;
                device.Address = args.BluetoothAddress;
                device.Name = name;
                device.SignalStrengthInDB = args.RawSignalStrengthInDBm;
                // Add / update Device to the Dictionary
                mDiscoveredDevices[args.BluetoothAddress] = device;
            }
            // debug 
            DeviceDiscovered(device);
            // Fire an event only if device discovered is not in list already
            // Firing an event will put the thread to sleep for some time to finish all the other stuff
            if (newDiscovery)
            {
                NewDeviceDiscovered(device);
            }
            }
            
            
        

        #endregion
        #region Public Methods
        // to Start discovering BLE Servers
        public void StartListening()
        {
            // If already Listening do nothing
            if (Listening)
                return;
            //else start the watcher object
            mWatcher.Start();
            // Inform everone that watcher has started listening
            StartedListening();
        }

        public void StopListening()
        {
            // If already not Listening do nothing
            if (!Listening)
                return;
            //else stop the watcher object
            mWatcher.Stop();
            // Inform everone that watcher has started listening
            // stop listening properly --> 
           // StoppedListening();
        }

        // task to pair with CABG
        public async Task PairToDevice(ulong Address)
        {
            Console.WriteLine("Came Inside the Pairing Task");
            var device = await BluetoothLEDevice.FromBluetoothAddressAsync(Address).AsTask();
            var handlerPairingRequested = new TypedEventHandler<DeviceInformationCustomPairing, DevicePairingRequestedEventArgs>(handlerPairingReq);
            if (device == null)
                throw new ArgumentNullException("Failed to get information about the device");
            bool CanPair = device.DeviceInformation.Pairing.CanPair;
            Console.Write($"This {device.Name} can pair result is {CanPair} \n");
            Console.Write($"This {device.Name} protection Level is {device.DeviceInformation.Pairing.ProtectionLevel} \n"); 
            if (!device.DeviceInformation.Pairing.IsPaired)
            {
                Console.Write($"{device.Name} Try Pairing");
                var result = await device.DeviceInformation.Pairing.Custom.PairAsync(DevicePairingKinds.ProvidePin, DevicePairingProtectionLevel.None);
                device.DeviceInformation.Pairing.Custom.PairingRequested += handlerPairingRequested;
                Console.Write($"{result.Status}");
            }
        }
        public void handlerPairingReq(DeviceInformationCustomPairing CP, DevicePairingRequestedEventArgs DPR)
        {
            //so we get here for custom pairing request.
            //this is the magic place where your pin goes.
            //my device actually does not require a pin but
            //windows requires at least a "0".  So this solved 
            //it.  This does not pull up the Windows UI either.
            DPR.Accept("22210");
 
        }
        public void PrintDictionary()
        {
            foreach (KeyValuePair < ulong, BLEDevice > kvp in mDiscoveredDevices)
            {
               Console.WriteLine($"Key = {kvp.Key}, Value = {kvp.Value}");
            }
        }
        #endregion
    }
}
