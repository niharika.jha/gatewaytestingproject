﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLE_AdvertisementListener
{
    public class BLEDevice
    {
        #region Public Properties

        // for recording broadcast time
        public DateTimeOffset BroadcastTime { get; set; }

        // for recording Adress

        public ulong Address { get; set; }

        // for recording the name of the device
        public string Name { get; set; }

        //for recording the Signal Strngth of the device

        public short SignalStrengthInDB { get; set; }

        // for recording the ID of the device
        public string DeviceID { get; set; }

        #endregion

        #region Constructor
        // Default Constructor
        public BLEDevice()
        {

        }
        #endregion

        // To String now overridden by string to give more infor for this class
        public override string ToString()
        {
            // debug code--
            return $"{(string.IsNullOrEmpty(Name) ? "[No Name]" : Name) } [{Address}] ({SignalStrengthInDB}) ";
        }
    }
}
